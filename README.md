# Planets

## Galaxy React App

<img src="https://gitlab.com/pascal.cantaluppi/planets/-/raw/main/frontend/public/planet.png" width="100" />
<br />
<br />

### Scripts

```console
cd .\backend
yarn strapi develop
```

Runs the strapi headless cms (backend)

```console
cd .\frontend
npm start
```

Runs the app in the development mode

```console
npm test
```

Launches the test runner

```console
npm run build
```

Builds the app for production.

### Deployment

```console
caprover deploy
```

Deploys the App to CapRover PaaS Management

`Frontend`

[https://planets.cantaluppi.app/](https://planets.cantaluppi.app/)

`Backend`

[https://strapi.cantaluppi.app/](https://strapi.cantaluppi.app/)
