import React, { createContext, useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import NotFound from "./components/NotFound";
import AppBar from "./components/AppBar";
import PlanetsTable from "./components/PlanetsTable";
import PlanetAdd from "./components/PlanetAdd";
import ResidentsTable from "./components/ResidentsTable";
import ResidentAdd from "./components/ResidentAdd";
import axios from "axios";
import "./App.css";

// api url and token
const url = process.env.REACT_APP_API_URL;
const payload = {
  headers: {
    Authorization: "Bearer " + process.env.REACT_APP_API_TOKEN,
  },
};

// use context api
export const PlanetContext = createContext();

const App = () => {
  // state for rest client
  const [loadingPlanets, setLoadingPlanets] = useState(true);
  const [loadingResidents, setLoadingResidents] = useState(true);
  const [error, setError] = useState(false);
  const [planets, setPlanets] = useState([]);
  const [residents, setResidents] = useState([]);

  // get planets from api
  useEffect(() => {
    const fetchPlanets = async () => {
      setLoadingPlanets(true);
      try {
        const { data: response } = await axios.get(
          url + "/planets?populate=*",
          payload
        );
        setPlanets(response.data);
      } catch (error) {
        console.error(error.message);
        setError(true);
      }
      setLoadingPlanets(false);
    };
    fetchPlanets();

    // get residents from api
    const fetchResidents = async () => {
      setLoadingResidents(true);
      try {
        const { data: response } = await axios.get(
          url + "/residents?populate=*",
          payload
        );
        setResidents(response.data);
      } catch (error) {
        console.error(error.message);
      }
      setLoadingResidents(false);
    };
    fetchResidents();
  }, []);

  // state for search
  const [search, setSearch] = useState("");

  // planets route
  const Planets = () => {
    return (
      <React.Fragment>
        <AppBar />
        <PlanetsTable planets={planets} search={search} />
        <PlanetAdd />
      </React.Fragment>
    );
  };

  // residents route
  const Residents = () => {
    return (
      <React.Fragment>
        <AppBar />
        <ResidentsTable residents={residents} search={search} />
        <ResidentAdd />
      </React.Fragment>
    );
  };

  // 404 route
  const NoMatch = () => <NotFound />;

  // error handling
  if (error) {
    return (
      <>
        <div className="Message">
          <p>This is bad</p>
        </div>
      </>
    );
  }

  // loading handling
  if (loadingPlanets || loadingResidents) {
    return (
      <>
        <div className="Message">
          <p>Loading..</p>
        </div>
      </>
    );
  }

  // return jsx
  return (
    <PlanetContext.Provider
      value={{
        search,
        setSearch,
      }}
    >
      <div>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={Planets()} />
            <Route path="/planets" strict={true} element={Planets()} />
            <Route path="/residents" strict={true} element={Residents()} />
            <Route path="*" element={NoMatch()} />
          </Routes>
        </BrowserRouter>
      </div>
    </PlanetContext.Provider>
  );
};

export default App;
