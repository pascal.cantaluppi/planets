import React, { useState } from "react";
import Table from "react-bootstrap/Table";
import PlanetsRow from "./PlanetsRow";

const PlanetsTable = (props) => {
  // planet state
  const [planets, setPlanets] = useState(props.planets);

  // compare column
  const compareBy = (key) => {
    return function (a, b) {
      let compare = 0;
      if (a.attributes[key] < b.attributes[key]) {
        compare = -1;
      } else if (a[key] > b[key]) {
        compare = 1;
      }
      return compare;
    };
  };

  // sort column
  const sortBy = (key) => {
    let planetList = [...planets];
    planetList.sort(compareBy(key));
    setPlanets(planetList);
  };

  return (
    <React.Fragment>
      <br />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th onClick={() => sortBy("id")} width="40px">
              #
            </th>
            <th onClick={() => sortBy("name")}>Name</th>
            <th onClick={() => sortBy("diameter")}>Diameter</th>
            <th onClick={() => sortBy("gravity")}>Gravity</th>
            <th onClick={() => sortBy("terrain")}>Terrain</th>
            <th onClick={() => sortBy("climate")}>Climate</th>
            <th width="10%">Residents</th>
          </tr>
        </thead>
        <tbody>{renderPlanets(planets, props.search)}</tbody>
      </Table>
    </React.Fragment>
  );
};

const renderPlanets = (planets, search) => {
  // return jsx
  return planets.map(({ attributes, id }, index) => {
    if (attributes.name.toLowerCase().includes(search.toLowerCase())) {
      return (
        <PlanetsRow
          key={index}
          id={id}
          name={attributes.name}
          diameter={attributes.diameter}
          gravity={attributes.gravity}
          terrain={attributes.terrain}
          climate={attributes.climate}
          residents={attributes.residents.data}
        />
      );
    } else {
      return <></>;
    }
  });
};

export default PlanetsTable;
