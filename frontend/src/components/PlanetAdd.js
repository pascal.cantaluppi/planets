import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";

const PlanetAdd = () => {
  // form validation state
  const [errors, setErrors] = useState(false);

  // planet state
  const [planetName, setPlanetName] = useState("");
  const [planetDiameter, setPlanetDiameter] = useState("");
  const [planetGravity, setPlanetGravity] = useState("");
  const [planetTerrain, setPlanetTerrain] = useState("");
  const [planetClimate, setPlanetClimate] = useState("");

  // planet event handlers
  const updatePlanetName = (e) => {
    setPlanetName(e.target.value);
  };
  const updatePlanetDiameter = (e) => {
    setPlanetDiameter(e.target.value);
  };
  const updatePlanetGravity = (e) => {
    setPlanetGravity(e.target.value);
  };
  const updatePlanetTerrain = (e) => {
    setPlanetTerrain(e.target.value);
  };
  const updatePlanetClimate = (e) => {
    setPlanetClimate(e.target.value);
  };

  // modal state
  const [show, setShow] = useState(false);

  // modal open handler
  const handleOpen = () => {
    setShow(true);
  };

  // modal close handler
  const handleClose = () => {
    setShow(false);
  };

  // post planet to api
  const handleSave = () => {
    if (planetName === "") {
      setErrors({ ...errors, name: "Name is required." });
      return;
    } else {
      setErrors({ ...errors, name: null });
      axios
        .post(
          process.env.REACT_APP_API_URL + "/planets",
          {
            data: {
              name: planetName,
              diameter: planetDiameter,
              gravity: planetGravity,
              terrain: planetTerrain,
              climate: planetClimate,
            },
          },
          {
            headers: {
              Authorization: "Bearer " + process.env.REACT_APP_API_TOKEN,
            },
          }
        )
        .catch(function (error) {
          console.error(error);
        });
      handleClose();
    }
  };

  // return jsx
  return (
    <React.Fragment>
      <div style={{ marginLeft: "10px" }}>
        <Button variant="outline-secondary" onClick={handleOpen}>
          Add new Planet
        </Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Planet</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputName"
            >
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                autoFocus
                onChange={updatePlanetName}
                isInvalid={!!errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputDiameter"
            >
              <Form.Label>Diameter</Form.Label>
              <Form.Control type="number" onChange={updatePlanetDiameter} />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputGravity"
            >
              <Form.Label>Gravity</Form.Label>
              <Form.Control type="text" onChange={updatePlanetGravity} />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputTerrain"
            >
              <Form.Label>Terrain</Form.Label>
              <Form.Control type="text" onChange={updatePlanetTerrain} />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputClimate"
            >
              <Form.Label>Climate</Form.Label>
              <Form.Control type="text" onChange={updatePlanetClimate} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="outline-secondary"
            onClick={handleSave}
            type="submit"
          >
            Add Planet
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default PlanetAdd;
