import React from "react";
import ResidentsShow from "./ResidentsShow";
//import ResidentsAssign from "./ResidentsAssign";

const PlanetsRow = (props) => {
  // return jsx
  return (
    <React.Fragment key={props.index}>
      <tr>
        <td width="40px">{props.id}</td>
        <td>{props.name}</td>
        <td>{props.diameter} km</td>
        <td>{props.gravity}</td>
        <td>{props.terrain}</td>
        <td>{props.climate}</td>
        <td width="10%">
          <ResidentsShow name={props.name} residents={props.residents} />
          {/* <table>
            <tbody>
              <tr>
                <td>
                  <ResidentsShow
                    name={props.name}
                    residents={props.residents}
                  />
                </td>
                <td>
                  <ResidentsAssign name={props.name} />
                </td>
              </tr>
            </tbody>
          </table> */}
        </td>
      </tr>
    </React.Fragment>
  );
};

export default PlanetsRow;
