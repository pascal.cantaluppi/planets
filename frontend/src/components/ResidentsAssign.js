import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import _ from "underscore";

const ResidentsAssign = () => {
  // modal state
  const [show, setShow] = useState(false);

  // residents state
  const [residents, setResidents] = useState([]);

  // modal event handlers
  const handleOpen = () => {
    setShow(true);
  };

  // modal close handler
  const handleClose = () => {
    setShow(false);
  };

  // add resident handler
  const handleAdd = () => {};

  // fetch residents
  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_URL + "/residents", {
        headers: {
          Authorization: "Bearer " + process.env.REACT_APP_API_TOKEN,
        },
      })
      .then(function (response) {
        setResidents(response.data.data);
      });
  }, []);

  // render residents
  const renderResidents = () => {
    if (residents.length === 0) {
      return <React.Fragment>No Residents</React.Fragment>;
    }
    let residentsList = [];
    for (let i = 0; i < residents.length; i++) {
      residentsList.push(residents[i].attributes.name);
    }
    residentsList = _.sortBy(residentsList);
    return residentsList.map((resident, index) => {
      return (
        <React.Fragment key={index}>
          <ListGroup.Item>
            <FontAwesomeIcon icon={faUserPlus} onClick={handleAdd} />
            &nbsp;&nbsp;{resident}
          </ListGroup.Item>
        </React.Fragment>
      );
    });
  };

  // return jsx
  return (
    <React.Fragment>
      <Button variant="light" onClick={handleOpen}>
        <FontAwesomeIcon icon={faUserPlus} />
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Resident</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ListGroup>{renderResidents(residents)}</ListGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default ResidentsAssign;
