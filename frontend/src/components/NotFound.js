import React from "react";

const NotFound = () => {
  // return jsx
  return (
    <React.Fragment>
      <div className="Message">
        <h1>404 - Page not found</h1>
        <p style={{ marginTop: 30 }}>No match for this Route</p>
        <p style={{ marginTop: 20 }}>
          <img
            alt="Planet"
            src="/planet.png"
            width="100"
            height="100"
            className="d-inline-block align-top"
          />
        </p>
      </div>
    </React.Fragment>
  );
};

export default NotFound;
