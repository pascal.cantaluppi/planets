import React, { useContext } from "react";
import { PlanetContext } from "../App";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";

const AppHeader = () => {
  // search state
  const { setSearch } = useContext(PlanetContext);

  // search event handler
  const updateSearch = (e) => {
    setSearch(e.target.value.toLowerCase());
  };

  // return jsx
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand>
          <img
            alt="Planet"
            src="/planet.png"
            width="35"
            height="35"
            className="d-inline-block align-top"
          />{" "}
          <b>Galaxy Database</b>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="planets">Planets</Nav.Link>
            <Nav.Link href="residents">Residents</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Text>
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          />
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Filter names"
              className="me-2"
              aria-label="Search"
              onChange={updateSearch}
            />
          </Form>
        </Navbar.Text>
      </Container>
    </Navbar>
  );
};

export default AppHeader;
