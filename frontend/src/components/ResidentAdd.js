import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";

const ResidentAdd = () => {
  // form validation state
  const [errors, setErrors] = useState(false);

  // resident state
  const [residentName, setResidentName] = useState("");
  const [residentBirthday, setResidentBirthday] = useState("");
  const [residentSize, setResidentSize] = useState("");
  const [residentSex, setResidentSex] = useState("");

  // resident event handlers
  const updateResidentName = (e) => {
    setResidentName(e.target.value);
  };
  const updateResidentBirthday = (e) => {
    setResidentBirthday(e.target.value);
  };
  const updateResidentSize = (e) => {
    setResidentSize(e.target.value);
  };
  const updateResidentSex = (e) => {
    setResidentSex(e.target.value);
  };

  // modal state
  const [show, setShow] = useState(false);

  // modal event handlers
  const handleOpen = () => {
    setShow(true);
  };

  // modal close handler
  const handleClose = () => {
    setShow(false);
  };

  // save resident handler
  const handleSave = () => {
    if (residentName === "") {
      setErrors({ ...errors, name: "Name is required." });
      return;
    } else {
      setErrors({ ...errors, name: null });
      axios
        .post(
          process.env.REACT_APP_API_URL + "/residents",
          {
            data: {
              name: residentName,
              birthday: residentBirthday,
              size: residentSize,
              sex: residentSex,
            },
          },
          {
            headers: {
              Authorization: "Bearer " + process.env.REACT_APP_API_TOKEN,
            },
          }
        )
        .then((response) => {
          console.log(response.data);
        })
        .catch(function (error) {
          console.error(error);
        });
      handleClose();
    }
  };

  // return jsx
  return (
    <React.Fragment>
      <div style={{ marginLeft: "10px" }}>
        <Button variant="outline-secondary" onClick={handleOpen}>
          Add new Resident
        </Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Resident</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputName"
            >
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                autoFocus
                onChange={updateResidentName}
                isInvalid={!!errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputBirthday"
            >
              <Form.Label>Birthday</Form.Label>
              <Form.Control type="date" onChange={updateResidentBirthday} />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputSize"
            >
              <Form.Label>Size</Form.Label>
              <Form.Control type="text" onChange={updateResidentSize} />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlInputSex"
            >
              <Form.Label>Sex</Form.Label>
              <Form.Control type="text" onChange={updateResidentSex} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="outline-secondary"
            onClick={handleSave}
            type="submit"
          >
            Add Resident
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default ResidentAdd;
