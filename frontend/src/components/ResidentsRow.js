import React from "react";
import dateFormat from "dateformat";
import PlanetsShow from "./PlanetsShow";

const ResidentsRow = (props) => {
  // return jsx
  return (
    <React.Fragment key={props.index}>
      <tr>
        <td width="40px">{props.id}</td>
        <td>{props.name}</td>
        <td>{dateFormat(props.birthday, "dd.mm.yyyy")}</td>
        <td>{props.size} m</td>
        <td>{props.sex}</td>
        <td width="10%">
          <PlanetsShow name={props.name} planets={props.planets} />
        </td>
      </tr>
    </React.Fragment>
  );
};

export default ResidentsRow;
