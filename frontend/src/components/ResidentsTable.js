import React, { useState } from "react";
import Table from "react-bootstrap/Table";
import ResidentsRow from "./ResidentsRow";

const ResidentsTable = (props) => {
  // resident state
  const [residents, setResidents] = useState(props.residents);

  // compare columns
  const compareBy = (key) => {
    return function (a, b) {
      let compare = 0;
      if (a.attributes[key] < b.attributes[key]) {
        compare = -1;
      } else if (a[key] > b[key]) {
        compare = 1;
      }
      return compare;
    };
  };

  // sort handler
  const sortBy = (key) => {
    let residentList = [...residents];
    residentList.sort(compareBy(key));
    setResidents(residentList);
  };

  // return jsx
  return (
    <React.Fragment>
      <br />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th onClick={() => sortBy("id")} width="40px">
              #
            </th>
            <th onClick={() => sortBy("name")}>Name</th>
            <th onClick={() => sortBy("birthday")}>Birthday</th>
            <th onClick={() => sortBy("size")}>Size</th>
            <th onClick={() => sortBy("sex")}>Sex</th>
            <th width="10%">Planets</th>
          </tr>
        </thead>
        <tbody>{renderResidents(residents, props.search)}</tbody>
      </Table>
    </React.Fragment>
  );
};

const renderResidents = (residents, search) => {
  return residents.map(({ attributes, id }, index) => {
    if (attributes.name.toLowerCase().includes(search.toLowerCase())) {
      return (
        <ResidentsRow
          key={index}
          id={id}
          name={attributes.name}
          birthday={attributes.birthday}
          size={attributes.size}
          sex={attributes.sex}
          planets={attributes.planets.data}
        />
      );
    } else {
      return <></>;
    }
  });
};

export default ResidentsTable;
