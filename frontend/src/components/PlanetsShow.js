import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import _ from "underscore";

const PlanetsShow = (props) => {
  // modal state
  const [show, setShow] = useState(false);

  // modal open handler
  const handleOpen = () => {
    setShow(true);
  };

  //modal close handler
  const handleClose = () => {
    setShow(false);
  };

  // render planets
  const renderPlanets = (planets) => {
    if (planets.length === 0) {
      return <React.Fragment>No Planes</React.Fragment>;
    }
    let planetsList = [];
    for (let i = 0; i < planets.length; i++) {
      planetsList.push(planets[i].attributes.name);
    }
    planetsList = _.sortBy(planetsList);
    return planetsList.map((planets, index) => {
      return (
        <React.Fragment key={index}>
          <li>{planets}</li>
        </React.Fragment>
      );
    });
  };

  // return jsx
  return (
    <React.Fragment>
      <Button variant="light">
        <FontAwesomeIcon icon={faMagnifyingGlass} onClick={handleOpen} />
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Planets {props.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul>{renderPlanets(props.planets)}</ul>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default PlanetsShow;
