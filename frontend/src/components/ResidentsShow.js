import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import _ from "underscore";

const ResidentsShow = (props) => {
  // modal state
  const [show, setShow] = useState(false);

  // modal event handlers
  const handleOpen = () => {
    setShow(true);
  };
  const handleClose = () => {
    setShow(false);
  };

  // Render residents
  const renderResidents = (residents) => {
    if (residents.length === 0) {
      return <React.Fragment>No Residents</React.Fragment>;
    }
    let residentsList = [];
    for (let i = 0; i < residents.length; i++) {
      residentsList.push(residents[i].attributes.name);
    }
    residentsList = _.sortBy(residentsList);
    return residentsList.map((resident, index) => {
      return (
        <React.Fragment key={index}>
          <li>{resident}</li>
        </React.Fragment>
      );
    });
  };

  // return jsx
  return (
    <React.Fragment>
      <Button variant="light" onClick={handleOpen}>
        <FontAwesomeIcon icon={faMagnifyingGlass} />
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Residents {props.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul>{renderResidents(props.residents)}</ul>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default ResidentsShow;
